# Бэкенд тестовое задание

### Запуск
Установка зависимостей
```shell
composer install
```

Переименовать .env.example в .env и проверить 14-16 строки

Миграция БД
```shell
php artisan migrate
```

Сид БД
```shell
php artisan db:seed
```

Запуск сервера
```shell
php artisan serve
```

### Роуты
```shell
php artisan route:list
```
